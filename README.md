# Framasites

## Support

Merci de [créer un ticket](https://framagit.org/framasoft/Framasite-framasite/issues/new) en étant le plus exhaustif possible. N'hésitez pas à ajouter des captures d'écran.

## Installation

### Prérequis

* `git`
* `nodejs >= v8.x` (mais `6.x` devrait fonctionner)
* `php >= 7.0`
* `PostgreSQL >= 9.6`

### Instructions

Ces instructions sont valables uniquement pour un environnement de développement.

* Cloner le dépôt ;
* Créer les dossiers à l'intérieur `blog/users` et `wiki/users` ;
* Télécharger grav et dokuwiki et les mettre dans les dossiers `grav` et `dokuwiki` ;
* `composer install` (installer les dépendances PHP - [installer Composer](https://getcomposer.org/)) Cette commande demandera également la valeur de nombreux paramètres ;
* `npm i` (installer les dépendances Javascript) ;
* Éditer dans `app/config/config.yml` les valeurs qui ne sont pas sous forme d'un paramètre (en cours de correction) ;
* `bin/console doctrine:database:create` (si base de données pas déjà créée, et utilisateur avec les droits)
* `bin/console doctrine:schema:create` ;
* `bin/console server:run` : rend Framasites accessible à l'adresse http://127.0.0.1:8080.

### Mise à jour

* `git pull` : récupération du dernier code
* `bin/console doctrine:migrations:migrate` migration de la base de données
* `bin/console cache:clear` Nettoie le cache
