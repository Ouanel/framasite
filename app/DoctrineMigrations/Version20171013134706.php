<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Add extra informations on Command objects for when the related fields have been deleted & rename payplug to mangopay & add mangopay_id to user entity
 */
class Version20171013134706 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE command ADD email VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE command ADD domain_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE command ADD username VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE command RENAME COLUMN payplug_id TO mangopay_id');
        $this->addSql('ALTER TABLE framasite_user ADD mangopay_id VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE command ADD payplug_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE command DROP username');
        $this->addSql('ALTER TABLE command DROP email');
        $this->addSql('ALTER TABLE command DROP domain_name');
        $this->addSql('ALTER TABLE command DROP mangopay_id');
        $this->addSql('ALTER TABLE framasite_user DROP mangopay_id');
    }
}
