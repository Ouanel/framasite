<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171018152053 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE framasite_user ADD active_command_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE framasite_user ADD CONSTRAINT FK_399E9F9F12430A6C FOREIGN KEY (active_command_id) REFERENCES command (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_399E9F9F12430A6C ON framasite_user (active_command_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');
        $this->addSql('ALTER TABLE framasite_user DROP CONSTRAINT FK_399E9F9F12430A6C');
        $this->addSql('DROP INDEX UNIQ_399E9F9F12430A6C');
        $this->addSql('ALTER TABLE framasite_user DROP active_command_id');
    }
}
