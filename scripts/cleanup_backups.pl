#!/usr/bin/perl
use strict;
use warnings;
use 5.10.0;

use Getopt::Long;
use Number::Bytes::Human qw(format_bytes);

my ($dir, $keep, $help, $dry_run, $quiet) = ('', 2);
GetOptions (
    "d|directory=s" => \$dir,
    "keep=i"        => \$keep,
    "help"          => \$help,
    "quiet"         => \$quiet,
    "n|dry-run"     => \$dry_run,
);

print_usage() if $help;

if ($dir eq '') {
    say "ERROR: You must give a directory\n";
    print_usage();
}

unless (-d $dir) {
    say "ERROR: $dir is not a directory\n";
    print_usage();
}

unless ($keep > 0) {
    say "ERROR: $keep is not a valid number (must be a non-null positive integer)\n";
    print_usage();
}

$dir .= '/' unless $dir =~ m#/$#;
$dir .= '*';

my @users = glob($dir);
my $size = 0;
for my $user (@users) {
    if (-d $user.'/backup/') {
        my @backups = glob($user.'/backup/*.zip');
        if (scalar(@backups) > $keep) {
            @backups = sort {
                my ($adev,$aino,$amode,$anlink,$auid,$agid,$ardev,$asize,$aatime,$amtime,$actime,$ablksize,$ablocks) = stat($a);
                my ($bdev,$bino,$bmode,$bnlink,$buid,$bgid,$brdev,$bsize,$batime,$bmtime,$bctime,$bblksize,$bblocks) = stat($b);
                return $amtime <=> $bmtime;
            } @backups;
            @backups = splice(@backups, 0, -$keep);
            if ($dry_run) {
                say "Those files would have been removed:" unless $quiet;
                for my $backup (@backups) {
                    say "\t$backup" unless $quiet;
                    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$fsize,$atime,$mtime,$ctime,$blksize,$blocks) = stat($backup);
                    $size += $fsize;
                }
            } else {
                for my $backup (@backups) {
                    my ($dev,$ino,$mode,$nlink,$uid,$gid,$rdev,$fsize,$atime,$mtime,$ctime,$blksize,$blocks) = stat($backup);
                    $size += $fsize;
                }
                unlink @backups or warn "Error while deleting backups in $user/backup: $!";
            }
        }
    }
}

if ($dry_run) {
    say '' unless $quiet;
    say sprintf('%s would have been freed', format_bytes($size));
} elsif (!$quiet) {
    say '';
    say sprintf('%s have been freed', format_bytes($size));
}

sub print_usage {
    say <<EOF;
cleanup_backups.pl (c) 2018 Framasoft, licensed under the terms of the AGPLv3.

Script to remove old backups from Grav's users directory if there is more than X backups.
Usage:
    ./cleanup_backups.pl -d|--directory <directory> [-k|--keep <int>] [-n|--dry-run] [-h|--help]
Options:
    -d|--directory <directory>      Grav's users directory
                                    mandatory, no default
    -k|--keep <int>                 positive integer, number of backups to keep
                                    optional, default to 2
    -n|--dry-run                    flag to run the script but don't remove files,
                                    prints the list of files that should have been removed
    -q|--quiet                      reduce verbosity
    -h|--help                       prints this help and exits
EOF
    exit;
}
