<?php

namespace AppBundle\Async;

use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Blog\BlogUser;
use AppBundle\Exception\SiteException\SiteCreationException;
use AppBundle\Exception\SiteException\SiteDeletionException;
use AppBundle\Exception\SiteException\UserSiteException\UserSiteDeletionException;
use AppBundle\Exception\SiteUserExistsException;
use AppBundle\Exception\SubDomainFolderExistsException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;
use Symfony\Component\Yaml\Yaml;

class AsyncAccountGrav extends AbstractMakeAccount implements AsyncAccountInterface
{
    /**
     * @var Blog
     */
    protected $site;

    /**
     * @var BlogUser
     */
    protected $siteUser;

    /**
     * Execute the async job
     *
     * @throws SiteCreationException
     */
    public function make()
    {
        try {
            $this->logger->info(
                'Preparing to create site subdomain ' . $this->site->getSubdomain(
                ) . 'and account ' . $this->siteUser->getUsername() . ' for user ' . $this->user->getUsername() . '!'
            );
            $this->checkSubdomain();
            $this->makeSubdomain();

            $this->makeSiteConfig();
            $this->makeAccount();

            $this->logger->info(
                'Created subdomain ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!'
            );
        } catch (SubDomainFolderExistsException $e) {
            $this->logger->error("The subdomain " . $e->getFolder() . " already exists. Aborting");
            throw new SiteCreationException();
        } catch (SiteUserExistsException $e) {
            $this->logger->error("The user " . $e->getSiteUser() . " already exists for site " . $e->getSiteFolder());
            throw new SiteCreationException();
        }
    }

    /**
     * Check that subdomain actually exists
     *
     * @param string|null $subdomain
     * @return bool
     * @throws SubDomainFolderExistsException
     */
    public function checkSubdomain(string $subdomain = null)
    {
        $this->logger->info('Checking subdomain ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
        $subdomainToCheck = null != $subdomain ? $subdomain : $this->site->getSubdomain();
        $fs = new Filesystem();
        $accountPath = $this->accountsPath . '/' . strtolower($subdomainToCheck);
        $this->logger->debug('Checked ' . $this->accountsPath . '/' . strtolower($subdomainToCheck) . ' path');
        if ($fs->exists($accountPath)) {
            throw new SubDomainFolderExistsException($accountPath, "Folder already exists");
        }
        return true;
    }

    /**
     * Create the subdomain and all necessary files
     */
    public function makeSubdomain()
    {
        $this->logger->info('Preparing subdomain creation ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
        $fs = new Filesystem();
        $accountPath = $this->accountsPath . '/' . strtolower($this->site->getSubdomain());
        $fs->mkdir($accountPath);

        $userData    = ['assets','backup','images', 'logs', 'cache', 'tmp', 'user', 'user/accounts', 'user/data', 'user/pages'];
        $blacklist   = ['.', '..', '.git'];

        // Relative symlink to grav's content
        if ($handle = opendir($this->enginePath)) {
            $blacklist = array_merge($blacklist, $userData);

            while (false !== ($file = readdir($handle))) {
                if (!in_array($file, $blacklist, true)) {
                    $fs->symlink($this->enginePath . '/' . $file, $this->accountsPath.'/' . $this->site->getSubdomain() . '/' . $file);
                }
            }
            closedir($handle);
        }
        // Create userData folders
        foreach ($userData as $file) {
            $fs->mkdir($accountPath . '/' . $file);
            $fs->chmod($this->accountsPath . '/' . $this->site->getSubdomain() . '/' . $file, 0755);
        }
        $fs->symlink($this->enginePath . '/user/plugins/', $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/plugins');
        $this->logger->info("Created plugins symlink for " . $this->site->getSubdomain() . ': ' . $this->accountsPath . '/../plugins/ to ' . $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/plugins');
        $fs->symlink($this->enginePath . '/user/themes/', $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/themes');
        $this->logger->info("Created theme symlink for " . $this->site->getSubdomain());


        $customHomePage = null;
        /** Pages */
        if (null === $this->site->getSiteModules() || empty($this->site->getSiteModules())) { // if we have no selections on modules, give them all
            $this->rcopy(
                $this->enginePath . '/user/pages/',
                $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/pages/'
            );
        } else {
            /** Copy common things */
            $this->rcopy(
                $this->enginePath . '/user/pages/common/',
                $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/pages/common'
            );
            $this->rcopy(
                $this->enginePath . '/user/pages/06.contact/',
                $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/pages/06.contact'
            );
            $this->rcopy(
                $this->enginePath . '/user/pages/images/',
                $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/pages/images'
            );

            /**
             * For each module, give what we need
             */
            foreach ($this->site->getSiteModules() as $module) {
                switch ($module) {
                    case Blog::MODULE_BLOG:
                        $this->rcopy(
                            $this->accountsPath . '/demo-blog/user/pages/04.blog/',
                            $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/pages/04.blog'
                        );
                        $customHomePage = '/blog';
                        break;
                    case Blog::MODULE_CV:
                        $this->rcopy(
                            $this->accountsPath . '/demo-cv/user/pages/05.a-propos/',
                            $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/pages/05.a-propos'
                        );
                        $customHomePage = '/a-propos';
                        break;
                    case Blog::MODULE_SINGLE_PAGE:
                        $this->rcopy(
                            $this->accountsPath . '/demo-page/user/pages/02.page_simple/',
                            $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/pages/02.page_simple'
                        );
                        $customHomePage = '/page_simple';
                        break;
                    case Blog::MODULE_SINGLE_PAGE_CARROUSEL:
                        $this->rcopy(
                            $this->accountsPath . '/demo-carousel/user/pages/01.page_carrousel/',
                            $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/pages/01.page_carrousel'
                        );
                        $customHomePage = '/page_carousel';
                        break;
                    default:
                        break;
                }
            }
        }

        /** Data */
        $this->rcopy($this->enginePath . '/user/data/', $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/data/');

        /** Config */
        $systemFilePath = $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/config/system.yaml';
        $fs->copy($this->enginePath . '/user/config/system.yaml', $systemFilePath);

        /**
         * Set homepage for module
         */
        if (null != $customHomePage) {
            $system = Yaml::parse(file_get_contents($systemFilePath));
            $system['home']['alias'] = $customHomePage;
            file_put_contents($systemFilePath, Yaml::dump($system));
        }

        $fs->symlink($this->enginePath . '/user/config/media.yaml', $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/config/media.yaml');
        $this->rcopy($this->enginePath . '/user/config/plugins/', $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/config/plugins/');
        $this->logger->info('Created subdomain ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
    }

    public function makeSiteConfig()
    {
        $this->logger->info('Creating config for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
        $siteConfig = [
            'title' => $this->site->getSiteName(),
            'default_lang' => $this->user->getLocale(),
            'author' => [
                'name' => $this->siteUser->getFullName(),
                'email' => $this->siteUser->getEmail(),
            ],
            'taxonomies' => ['category', 'tag'],
            'metadata' => [
                'description' => $this->site->getSiteDescription(),
                'keywords' => $this->site->getSiteKeywords(),
            ],
            'summary' => [
                'enabled' => true,
                'format' => 'short',
                'size' => 300,
                'delimiter' => '===',
            ],
            'redirects' => [
                '/internal' => '/gravstrap-theme-versatile-internal-template[303]',
            ],
            'blog' => [
                'route' => '/blog',
            ],
        ];

        $yamlData = Yaml::dump($siteConfig);

        $fs = new Filesystem();
        if (!$fs->exists($this->accountsPath . '/' . $this->site->getSubdomain() . '/user/config')) {
            $fs->mkdir($this->accountsPath . '/' . $this->site->getSubdomain() . '/user/config');
        }

        file_put_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '/user/config/site.yaml', $yamlData);
        $this->logger->info('Created main config for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');

        $emailConfig = file_get_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '/user/config/plugins/email.yaml');

        $emailData = Yaml::parse($emailConfig);

        $emailData['to'] = $this->siteUser->getEmail();
        $emailData['from'] = 'no-reply@frama.site';

        $emailConfig = Yaml::dump($emailData);
        file_put_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '/user/config/plugins/email.yaml', $emailConfig);
        $this->logger->info('Created email config for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
    }

    /**
     * @param string|null $username
     * @param string|null $subdomain
     * @return bool
     * @throws SiteUserExistsException
     */
    public function checkAccount(string $username = null, string $subdomain = null)
    {
        $usernameToCheck = null != $username ? $username : $this->siteUser->getUsername();
        $subdomainToCheck = null != $subdomain ? $subdomain : $this->site->getSubdomain();
        $this->logger->info('Checking account ' . $usernameToCheck . ' for site ' . $subdomainToCheck . ' for user ' . $this->user->getUsername() . '!');
        $fs = new Filesystem();
        if ($fs->exists($this->accountsPath . '/' . $subdomainToCheck . '/user/accounts/' . $usernameToCheck . '.yaml')) {
            throw new SiteUserExistsException($usernameToCheck, "User already exists !");
        }
        return true;
    }

    /**
     * Create an user account
     *
     * @throws SiteUserExistsException
     */
    public function makeAccount()
    {
        $this->logger->info('Creating account ' . $this->siteUser->getUsername() . ' for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
        $this->checkAccount();
        $admin = $this->siteUser->isAdmin();

        $fs = new Filesystem();

        /**
         * Eventutally create the avatars folder
         */
        $avatarsPath = $this->accountsPath . '/' . $this->site->getSubdomain() . '/user/accounts/avatars/';
        if (!$fs->exists($avatarsPath)) {
            $fs->mkdir($avatarsPath);
        }

        /**
         * Copy avatar
         */
        $avatarPath =  'user/accounts/avatars/xVTJub3GlASrgzw.jpg';

        $fs->copy($this->enginePath . '/' . $avatarPath, $avatarsPath . 'xVTJub3GlASrgzw.jpg');

        $userData = [
            'hashed_password' => password_hash($this->siteUser->getPassword(), PASSWORD_DEFAULT),
            'email' => $this->isValidString($this->siteUser->getEmail()) ? $this->siteUser->getEmail() : $this->user->getEmail(),
            'language' => $this->isValidString($this->siteUser->getLocale()) ? $this->siteUser->getLocale() : $this->user->getLocale(),
            'title' => 'Administrator',
            'access' => [
                'admin' => [
                    'super' => $admin,
                    'login' => true,
                    'cache' => false,
                    'configuration' => false,
                    'configuration_system' => false,
                    'configuration_site' => false,
                    'configuration_media' => false,
                    'configuration_info' => false,
                    'settings' => true,
                    'pages' => true,
                    'maintenance' => false,
                    'statistics' => true,
                    'plugins' => true,
                    'themes' => true,
                    'users' => $admin,
                ],
                'site' => [
                    'login' => true,
                    'editable' => true,
                ],
            ],
            'fullname' => $this->siteUser->getUsername(),
            'state' => 'enabled',
            'avatar' => [
                $avatarPath => [
                    'name' => $this->siteUser->getUsername() . '.jpg',
                    'type' => 'image/jpeg',
                    'size' => 62518,
                    'path' => $avatarPath
                ]
            ]
        ];
        $yamlData = Yaml::dump($userData, 3);
        file_put_contents($this->accountsPath . '/' . $this->site->getSubdomain() . '/user/accounts/' . $this->siteUser->getUsername() . '.yaml', $yamlData);

        $this->logger->info('Created account ' . $this->siteUser->getUsername() . ' for site ' . $this->site->getSubdomain() . ' for user ' . $this->user->getUsername() . '!');
    }

    /**
     * @throws SiteDeletionException
     */
    public function deleteSite()
    {
        $this->logger->info('Deleting site at subdomain ' . $this->site->getSubdomain() . '.frama.site' . '...');
        $fs = new Filesystem();
        $this->logger->info('Deleting site at subdomain ' . $this->site->getSubdomain() . '...');
        if (null == $this->site->getSubdomain() || $this->site->getSubdomain() == '') { // TODO : check against ../../ and such things
            throw new SiteDeletionException("Subdomain is empty, won't delete everything !");
        }
        $fs->remove($this->accountsPath . '/' . $this->site->getSubdomain());
    }

    /**
     * Deletes an user from a site
     *
     * @throws UserSiteDeletionException
     */
    public function deleteSiteUser()
    {
        $this->logger->info($this->user->getUsername() . " tried to delete account " . $this->siteUser->getUsername() . " for website " . $this->site->getSubdomain() . '.frama.site');

        $fs = new Filesystem();
        $finder = new Finder();

        if (!$fs->exists($this->accountsPath . '/' . $this->site->getSubdomain() . '/user/accounts/' . $this->siteUser->getUsername() . '.yaml')) {
            throw new UserSiteDeletionException("This user doesn't exist. Strange");
        }
        $files = $finder->files()->in($this->accountsPath . '/' . $this->site->getSubdomain() . '/user/accounts/')->name('*.yaml');
        foreach ($files as $file) {
            /** @var SplFileInfo $file */
            $userData = Yaml::parse($file->getContents());

            $this->logger->debug("Testing if account " . $file->getRelativePathname() . " is admin");

            /** There's at least one more admin, we can remove this user */
            if (isset($userData['access']['admin']) && null !== $userData['access']['admin'] && $file->getRelativePathname() !== $this->siteUser->getUsername()) {
                $this->logger->debug("The site user " . $file . " is an admin, so we can delete the site user " . $this->siteUser->getUsername());
                $fs->remove($this->accountsPath . '/' . $this->site->getSubdomain() . '/user/accounts/' . $this->siteUser->getUsername() . '.yaml');

                $this->logger->info($this->user->getUsername() . " deleted account " . $this->siteUser->getUsername() . " for website " . $this->site->getSubdomain() . '.frama.site');
            }
        }
    }
}
