<?php

namespace AppBundle\Async;

use AppBundle\Exception\SiteException\UserSiteException\UserSiteDeletionException;

interface AsyncAccountInterface extends AsyncInterface
{
    /**
     * Do checks, make subdomain and make account
     */
    public function make();

    /**
     * Check that a subdomain is available
     * @param string|null $subdomain
     */
    public function checkSubdomain(string $subdomain = null);

    /**
     * Make a subdomain
     */
    public function makeSubdomain();

    /**
     * Check that an account is available
     *
     * @param string|null $username
     * @param string|null $subdomain
     * @return bool
     */
    public function checkAccount(string $username = null, string $subdomain = null);

    /**
     * Make an account
     */
    public function makeAccount();

    /**
     * Delete a site
     */
    public function deleteSite();

    /**
     * Delete user from site
     *
     * @throws UserSiteDeletionException
     */
    public function deleteSiteUser();

    /**
     * List sites
     */
    public function listSites();
}
