<?php

namespace AppBundle\Async\Gandi;

use AppBundle\Entity\CommandDomain;
use AppBundle\Entity\Contact;
use AppBundle\Helper\DomainFactory;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use PhpAmqpLib\Message\AMQPMessage;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;

class CheckContactConsumer implements ConsumerInterface
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var ProducerInterface
     */
    private $producer;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $entityManager, DomainFactory $domainFactory, Serializer $serializer, ProducerInterface $producer)
    {
        $this->logger = $logger;
        $this->entityManager = $entityManager;
        $this->domainFactory = $domainFactory;
        $this->serializer = $serializer;
        $this->producer = $producer;
    }

    /**
     * @param AMQPMessage $msg The message
     * @return mixed false to reject and requeue, any other value to acknowledge
     */
    public function execute(AMQPMessage $msg)
    {
        $this->logger->debug('Message received is', [$msg->getBody()]);
        /** @var CommandDomain $commandDomain */
        $commandDomain = $this->serializer->deserialize($msg->getBody(), CommandDomain::class, 'json');
        $contact = $commandDomain->getDomain()->getContact('owner');

        if (!($commandDomain instanceof CommandDomain && $contact instanceof Contact)) {
            $this->logger->error('Wrong message for CreateContactConsumer');
            return false;
        }

        $this->logger->info("Getting create contact operation to check if it's finished", [$contact->getId()]);
        $operation = $this->domainFactory->getOperationsList([
            'contact_id' => (int) $contact->getId(),
            'type' => 'contact_create',
        ])[0];

        $this->logger->info("This is the contact", [$operation]);
        if ($operation['step'] === 'DONE') {
            $this->logger->info('User creation is now done');
            /**
             * Save new (and confirmed) Gandi ID for User
             */
            $user = $this->entityManager->getRepository('AppBundle:User')->find($commandDomain->getCommand()->getUser()->getId());
            $user->setGandiId($contact->getGandiId());
            $this->entityManager->persist($user);
            $this->entityManager->flush();
            $this->logger->info('Saved gandiId for user ' . $user->getId());

            /**
             * Get the domain
             */
            $this->producer->publish($this->serializer->serialize($commandDomain, 'json', SerializationContext::create()->setGroups(['domain_register'])));
            $this->logger->info('Launched the registration for the domain');
            return true;
        }
        return false;
    }
}
