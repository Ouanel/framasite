<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Exception\Payment\PaymentException;
use AppBundle\Form\SettingsType;
use AppBundle\Helper\PaymentFactory;
use AppBundle\Service\StripeKeyService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use UserBundle\Form\ChangePasswordType;
use UserBundle\Form\UserInformationType;

class SettingsController extends Controller
{
    /**
     * @var PaymentFactory
     */
    private $paymentFactory;

    /**
     * @var StripeKeyService
     */
    private $stripeKeyService;

    /**
     * SettingsController constructor.
     * @param PaymentFactory $paymentFactory
     * @param StripeKeyService $stripeKeyService
     */
    public function __construct(PaymentFactory $paymentFactory, StripeKeyService $stripeKeyService)
    {
        $this->paymentFactory = $paymentFactory;
        $this->stripeKeyService = $stripeKeyService;
    }

    /**
     * @Route("/settings", name="settings")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexConfigAction(Request $request): Response
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        // handle basic config details
        $configForm = $this->createForm(SettingsType::class, $this->getUser());
        $configForm->handleRequest($request);

        if ($configForm->isSubmitted() && $configForm->isValid()) {
            $em->persist($user);
            $em->flush();

            $request->getSession()->set('_locale', $this->getUser()->getLocale());

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('flashes.settings.config_saved', [], 'messages')
            );

            $this->get('logger')->info('Locale was changed for user ' . $user->getUsername());

            return $this->redirect($this->generateUrl('settings'));
        }

        return $this->render('default/settings/config.html.twig', ['form' => $configForm->createView(), 'buy_domains' => $this->getParameter('app.framasoft.buy_domains')]);
    }

    /**
     * @Route("/settings/profile", name="settings-profile")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexProfileAction(Request $request): Response
    {
        $user = $this->getUser();
        // Handle profile
        $userManager = $this->container->get('fos_user.user_manager');
        // handle changing user information
        $userForm = $this->createForm(UserInformationType::class, $user);
        $userForm->handleRequest($request);

        if ($userForm->isSubmitted() && $userForm->isValid()) {
            $this->get('logger')->debug('preparing to save user');
            $userManager->updateUser($user, true);

            $this->get('logger')->info('User ' . $user->getUsername() . ' updated his profile');

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('flashes.settings.profile_updated', [], 'messages')
            );

            return $this->redirect($this->generateUrl('settings-profile'));
        }
        return $this->render('default/settings/profile.html.twig', ['form' => $userForm->createView(), 'buy_domains' => $this->getParameter('app.framasoft.buy_domains')]);
    }

    /**
     * @Route("/settings/password", name="settings-password")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function indexPasswordAction(Request $request): Response
    {
        $user = $this->getUser();
        $userManager = $this->container->get('fos_user.user_manager');

        // handle changing password
        $pwdForm = $this->createForm(ChangePasswordType::class, null);
        $pwdForm->handleRequest($request);

        if ($pwdForm->isSubmitted() && $pwdForm->isValid()) {
            $user->setPlainPassword($pwdForm->get('new_password')->getData());
            $userManager->updateUser($user, true);
            $this->get('logger')->info('User ' . $user->getUsername() . ' updated his password');

            $this->get('session')->getFlashBag()->add('success', $this->get('translator')->trans('flashes.settings.password_updated', [], 'messages'));

            return $this->redirect($this->generateUrl('settings-password'));
        }
        return $this->render('default/settings/password.html.twig', ['form' => $pwdForm->createView(), 'buy_domains' => $this->getParameter('app.framasoft.buy_domains')]);
    }

    /**
     * @Route("/settings/card", name="settings-card")
     *
     * @return Response
     */
    public function indexCardsAction(): Response
    {
        $cards = [];
        $defaultCardId = null;
        if ($stripeUserId = $this->getUser()->getStripeId()) {
            $cards = $this->paymentFactory->listCards($stripeUserId);
            $defaultCardId = $this->paymentFactory->getUser($stripeUserId)->default_source;
        }
        return $this->render('default/settings/cards/index.html.twig', [
            'cards' => $cards,
            'default_card_id' => $defaultCardId,
            'buy_domains' => $this->getParameter('app.framasoft.buy_domains'),
            'stripe_public_key' => $this->stripeKeyService->getPublicKey(),
        ]);
    }

    /**
     * @Route("/settings/card/new/save", name="settings-card-new")
     * @param Request $request
     * @return RedirectResponse
     * @throws \Exception
     */
    public function addNewCardAction(Request $request): RedirectResponse
    {
        /**
         * Finalize the registration
         */
        try {
            if (!$request->request->has('stripeToken')) {
                $this->get('logger')->error('Mangopay returned an empty response for data when registering a card');
                throw new PaymentException("Data returned was empty");
            }

            $token = $request->request->get('stripeToken');

            /** @var User $user */
            $user = $this->getUser();
            if (!$user->getStripeId()) {
                $customer = $this->paymentFactory->createUser($user->getEmail(), $token);
                $user->setStripeId($customer->id);
                $this->getDoctrine()->getManager()->persist($user);
                $this->getDoctrine()->getManager()->flush();
            }

            $this->paymentFactory->createCard($user->getStripeId(), $token);

            $this->get('logger')->info('Saved the card ID');

            $this->get('session')->getFlashBag()->add(
                'success',
                $this->get('translator')->trans('flashes.settings.cards.new.success', [], 'messages')
            );
        } catch (PaymentException $e) {
            $this->get('logger')->error('An error occured when registring a card : ' . $e->getMessage());
            $this->get('session')->getFlashBag()->add(
                'danger',
                $this->get('translator')->trans('flashes.settings.cards.new.error', ['%error%' => $e->getMessage()], 'messages')
            );
        } catch (\Exception $e) {
            $this->get('logger')->error('An error occured when registring a card : ' . $e->getMessage());
        }

        return $this->redirectToRoute('settings-card');
    }

    /**
     * @Route("/settings/card/{cardId}/remove", name="settings-card-remove")
     *
     * @param string $cardId
     * @return RedirectResponse
     */
    public function removeCardAction(string $cardId)
    {
        /**
         * Deactivate card on Stripe's side
         */
        $stripeCard = $this->paymentFactory->removeCard($this->getUser()->getStripeId(), $cardId);

        return $this->redirectToRoute('settings-card');
    }
}
