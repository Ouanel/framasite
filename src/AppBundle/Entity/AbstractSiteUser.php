<?php

namespace AppBundle\Entity;

use AppBundle\Validator\Constraints as FramasitesAssert;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @FramasitesAssert\ConstraintValidUserSiteConfiguration()
 */
class AbstractSiteUser
{
    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Regex(pattern="/^[a-z0-9]+$/", message="site.user.lowercase_username")
     */
    protected $username;

    /**
     * @var string
     * @Assert\NotBlank()
     * @Assert\Length(min="8")
     */
    protected $password;

    /**
     * @var Site
     */
    protected $site;

    /**
     * AbstractSiteUser constructor.
     * @param Site|null $site
     */
    public function __construct(Site $site = null)
    {
        $this->site = $site;
    }

    /**
     * @return string
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return AbstractSiteUser
     */
    public function setUsername($username): AbstractSiteUser
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param string $password
     * @return AbstractSiteUser
     */
    public function setPassword(string $password): AbstractSiteUser
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return Site
     */
    public function getSite()
    {
        return $this->site;
    }

    /**
     * @param Site $site
     * @return AbstractSiteUser
     */
    public function setSite(Site $site): AbstractSiteUser
    {
        $this->site = $site;
        return $this;
    }

    /**
     * @return string
     */
    public function __toString(): string
    {
        return $this->username;
    }
}
