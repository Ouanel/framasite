<?php

namespace AppBundle\Entity\SinglePage;

use AppBundle\Entity\AbstractSiteUser;

class SinglePageUser extends AbstractSiteUser
{
    /**
     * @var string
     */
    protected $locale;

    /**
     * @return string
     */
    public function getLocale()
    {
        return $this->locale;
    }

    /**
     * @param string $locale
     * @return AbstractSiteUser
     */
    public function setLocale(string $locale): AbstractSiteUser
    {
        $this->locale = $locale;
        return $this;
    }
}
