<?php

namespace AppBundle\Entity\Zone;

use Doctrine\Common\Collections\ArrayCollection;

class Zone
{

    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var int
     */
    private $nbDomains;

    /**
     * @var int
     */
    private $currentVersion;

    /**
     * @var ArrayCollection
     */
    private $versions;

    /**
     * Zone constructor.
     * @param int $id
     */
    public function __construct(int $id = 0)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return Zone
     */
    public function setId(int $id): Zone
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return Zone
     */
    public function setName(string $name): Zone
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return ArrayCollection
     */
    public function getVersions(): ArrayCollection
    {
        return $this->versions;
    }

    /**
     * @param ArrayCollection $versions
     * @return Zone
     */
    public function setVersions(ArrayCollection $versions): Zone
    {
        $this->versions = $versions;
        return $this;
    }

    /**
     * @return int
     */
    public function getNbDomains(): int
    {
        return $this->nbDomains;
    }

    /**
     * @param int $nbDomains
     * @return Zone
     */
    public function setNbDomains(int $nbDomains): Zone
    {
        $this->nbDomains = $nbDomains;
        return $this;
    }

    /**
     * @return int
     */
    public function getCurrentVersion(): int
    {
        return $this->currentVersion;
    }

    /**
     * @param int $currentVersion
     * @return Zone
     */
    public function setCurrentVersion(int $currentVersion): Zone
    {
        $this->currentVersion = $currentVersion;
        return $this;
    }
}
