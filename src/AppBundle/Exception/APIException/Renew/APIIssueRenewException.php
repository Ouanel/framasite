<?php

namespace AppBundle\Exception\APIException\Renew;

use AppBundle\Exception\APIException\RenewException;

class APIIssueRenewException extends RenewException
{
}
