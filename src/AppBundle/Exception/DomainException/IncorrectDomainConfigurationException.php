<?php

namespace AppBundle\Exception\DomainException;

use Exception;

class IncorrectDomainConfigurationException extends Exception
{
    /** @var int */
    private $ipType; // ip v4 or v6

    /** @var int */
    private $errorType; // missing or wrong

    const ERROR_TYPE_MISSING = 1;
    const ERROR_TYPE_WRONG = 2;
    const ERROR_TYPE_RESERVED = 3;

    const IP_V4 = 4;
    const IP_V6 = 6;

    public function __construct(int $errorType, int $ipType = null, string $message = "")
    {
        parent::__construct($message);
        $this->errorType = $errorType;
        $this->ipType = $ipType;
    }

    /**
     * @return int
     */
    public function getErrorType(): int
    {
        return $this->errorType;
    }

    /**
     * @return int
     */
    public function getIpType(): int
    {
        return $this->ipType;
    }
}
