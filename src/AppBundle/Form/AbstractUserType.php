<?php

namespace AppBundle\Form;

use AppBundle\Entity\AbstractSiteUser;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\Length;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Regex;

abstract class AbstractUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('email', EmailType::class, [
                'label' => 'site.new.user.email',
                'label_attr' => ['class' => 'col-sm-2'],
                'data' => $options['user']->getEmail(),
            ])
            ->add('username', TextType::class, [
                'label' => 'site.new.user.login',
                'label_attr' => ['class' => 'col-sm-2'],
                'data' => $options['user']->getUsername(),
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => 'site.user.password.match',
                'first_options' => ['label' => 'site.new.user.password', 'label_attr' => ['class' => 'col-sm-2']],
                'second_options' => ['label' => 'site.new.user.password_repeated', 'label_attr' => ['class' => 'col-sm-2']],
                'constraints' => [
                    new Length([
                           'min' => 8,
                           'minMessage' => 'site.user.password.too_short',
                       ]),
                    new NotBlank(),
                ],
                'label' => 'site.new.user.password',
            ])
            ->add('fullName', TextType::class, [
                'label' => 'site.new.user.fullname',
                'label_attr' => ['class' => 'col-sm-2'],
                'required' => false,
            ])
            ->add('admin', CheckboxType::class, [
                'label' => 'site.new.user.admin',
                'label_attr' => ['class' => 'col-sm-offset-2'],
                'data' => true,
                'required' => false,
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
           'data_class' => AbstractSiteUser::class,
            'inherit_data' => true,
            'user' => null,
       ]);
    }
}
