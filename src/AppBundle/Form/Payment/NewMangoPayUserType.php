<?php

namespace AppBundle\Form\Payment;

use AppBundle\Entity\Payment\MangoPayUser;
use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NewMangoPayUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        /** @var User $user */
        $user = $options['user'];
        $builder->add('email', EmailType::class, [
            'label_attr' => ['class' => 'col-sm-4'],
            'label' => 'settings.profile.email.label',
            'data' => $user->getEmail(),
        ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'settings.profile.type.user' => User::USER_TYPE_NATURAL,
                    'settings.profile.type.business' => User::USER_TYPE_LEGAL_BUSINESS,
                    'settings.profile.type.organization' => User::USER_TYPE_LEGAL_ORGANIZATION,
                ],
                'label' => 'settings.profile.type.label',
                'expanded' => true,
                'multiple' => false,
                'label_attr' => ['class' => 'col-sm-12'],
                'empty_data' => User::USER_TYPE_NATURAL,
                'required' => true,
                'data' => $user->getType(),
            ])
            ->add('legalName', TextType::class, [
                'label_attr' => ['class' => 'col-sm-4'],
                'label' => 'settings.profile.legal_name.label',
                'data' => $user->getLegalName(),
            ])
            ->add('firstName', TextType::class, [
            'label_attr' => ['class' => 'col-sm-4'],
                'label' => 'settings.profile.first_name.label',
                'data' => $user->getFirstName(),
        ])
            ->add('lastName', TextType::class, [
                'label_attr' => ['class' => 'col-sm-4'],
                'label' => 'settings.profile.last_name.label',
                'data' => $user->getLastName(),
            ])
            ->add('birthday', BirthdayType::class, [
                'label_attr' => ['class' => 'col-sm-4'],
                'attr' => ['autocomplete' => 'bday'],
                'label' => 'settings.profile.birthday.label',
                'years' => range(date('Y'), date('Y') - 120),
                'data' => $user->getBirthday(),
            ])
            ->add('country', CountryType::class, [
                'label_attr' => ['class' => 'col-sm-4'],
                'label' => 'settings.profile.country.label',
                'data' => $user->getCountry(),
                'attr' => ['autocomplete' => 'country'],
                'preferred_choices' => ['FR', 'GB', 'US', 'DE'],
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-success'],
                'label' => 'settings.save'
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
                                'data_class' => MangoPayUser::class,
                                'user' => null,
                               ]);
    }

    public function getBlockPrefix()
    {
        return null;
    }
}
