<?php

namespace AppBundle\Form\SinglePage;

use AppBundle\Entity\SinglePage\SinglePage;
use AppBundle\Form\SiteType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SinglePageType extends SiteType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('siteName', TextType::class, [
            'label' => 'site.new.informations.site_name.label',
            'attr' => ['placeholder' => 'site.new.informations.site_name.placeholder'],
            'label_attr' => ['class' => 'col-sm-2'],
        ])
        ->add('siteDescription', TextareaType::class, [
            'label' => 'site.new.informations.site_description.label',
            'attr' => ['placeholder' => 'site.new.informations.site_description.placeholder'],
            'label_attr' => ['class' => 'col-sm-2'],
        ])
        ->add('siteKeywords', TextType::class, [
            'label' => 'site.new.informations.site_keywords.label',
            'attr' => ['placeholder' => 'site.new.informations.site_keywords.placeholder'],
            'label_attr' => ['class' => 'col-sm-2'],
        ])
        ->add('siteUsers', SinglePageUserType::class, [
            'lang_choices' => $options['lang_choices'],
            'user' => $options['user'],
        ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SinglePage::class,
            'lang_choices' => null,
            'user' => null,
        ]);
    }
}
