<?php

namespace AppBundle\Form\Zone;

use AppBundle\Entity\Zone\Record;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RecordType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        parent::buildForm($builder, $options);
        $builder->add('name', TextType::class, [])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    "A" => 'A',
                    "AAAA" => 'AAAA',
                    "CNAME" => 'CNAME',
                    "MX" => 'MX',
                    "NS" => 'NS',
                    "TXT" => 'TXT',
                    "WKS" => 'WKS',
                    "SRV" => 'SRV',
                    "LOC" => 'LOC',
                    "SPF" => 'SPF',
                    "CAA" => 'CAA',
                ]
            ])
            ->add('value', TextType::class, [])
            ->add('ttl', NumberType::class, [])
            ->add('save', SubmitType::class, [])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Record::class,
        ]);
    }
}
