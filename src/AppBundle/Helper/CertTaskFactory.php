<?php

namespace AppBundle\Helper;

use AppBundle\Entity\Blog\Blog;
use AppBundle\Entity\Certificate\CertData;
use AppBundle\Entity\Certificate\CertTask;
use AppBundle\Entity\Domain;
use AppBundle\Entity\SinglePage\SinglePage;
use AppBundle\Entity\User;
use AppBundle\Entity\Wiki\Wiki;
use AppBundle\Exception\SiteException\UnknownSiteType;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class CertTaskFactory
{

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger, EntityManagerInterface $em, Connection $connection)
    {
        $this->logger = $logger;
        $this->em = $em;
        $this->connection = $connection;
    }

    /**
     * @param User $user
     * @param Domain $domain
     * @param int $action
     * @param int $status
     * @param int|null $next
     * @return int
     * @throws UnknownSiteType
     */
    public function createCertTaskForAttachment(
        User $user,
                                                Domain $domain,
                                                int $action = CertData::ACTION_CREATE,
                                                int $status = CertTask::STATUS_TODO,
                                                int $next = null
    ) {
        $site = $domain->getSite();

        $certData = new CertData();
        $certData->setSubdomain($site->getSubdomain())
            ->setAction($action)
            ->setUserLocale($user->getLocale())
            ->setUserEmail($user->getEmail());

        foreach ($site->getDomains() as $domain) {
            $certData->addName($domain);
        }

        if ($site instanceof Blog) {
            $certData->setType(CertData::TYPE_SITE);
            $certData->setSuffix('.frama.site');
        } elseif ($site instanceof SinglePage) {
            $certData->setType(CertData::TYPE_SINGLE);
            $certData->setSuffix('.frama.site');
        } elseif ($site instanceof Wiki) {
            $certData->setType(CertData::TYPE_WIKI);
            $certData->setSuffix('.frama.wiki');
        } else {
            throw new UnknownSiteType('Ooops');
        }

        /**
         * Set next task to perform
         */
        if (null !== $next) {
            $certData->setNext($next);
        }

        $certTask = new CertTask($user);
        $certTask->setStatus($status)->setTaskData($certData);
        $this->em->persist($certTask);
        $this->em->flush();
        $taskId = $certTask->getId();

        try {
            /**
             * Notify PgSQL
             */
            /** @var Connection $connection */
            if ($this->connection->getDatabasePlatform()->getName() === 'postgresql') {
                $stmt = $this->connection->prepare("SELECT pg_notify('certbot_channel', :payload)");
                $stmt->execute(['payload' => $taskId]);
            }
        } catch (DBALException $e) {
            $this->logger->error("Failed to perform pg_notify for CertTask " . $taskId . " : " . $e->getMessage());
        }

        return $taskId;
    }
}
