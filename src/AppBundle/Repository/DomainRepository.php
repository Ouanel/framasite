<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

class DomainRepository extends EntityRepository
{
    /**
     * Count how many domains are enabled.
     *
     * @return int
     */
    public function getSumActiveDomains()
    {
        return $this->createQueryBuilder('d')
            ->select('count(d)')
            ->andWhere('d.status = 100')
            ->getQuery()
            ->getSingleScalarResult();
    }

    /**
     * Count how many domains are enabled.
     *
     * @return int
     */
    public function getSumActiveDomainsByFrama()
    {
        return $this->createQueryBuilder('d')
            ->select('count(d)')
            ->andWhere('d.status = 100')
            ->andWhere('d.registeredByFrama = true')
            ->getQuery()
            ->getSingleScalarResult();
    }
}
