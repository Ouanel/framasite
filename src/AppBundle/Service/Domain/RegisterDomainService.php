<?php

namespace AppBundle\Service\Domain;

use AppBundle\Entity\CommandDomain;
use AppBundle\Entity\Contact;
use AppBundle\Entity\Domain;
use AppBundle\Helper\DomainFactory;
use AppBundle\Service\BaseService;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\Serializer;
use OldSound\RabbitMqBundle\RabbitMq\ProducerInterface;
use Psr\Log\LoggerInterface;

class RegisterDomainService extends BaseService
{
    /**
     * @var DomainFactory
     */
    private $domainFactory;

    /**
     * @var string
     */
    private $framaGandiId;

    /**
     * @var ProducerInterface
     */
    private $producer;

    /**
     * @var Serializer
     */
    private $serializer;

    public function __construct(
        LoggerInterface $logger,
        EntityManagerInterface $entityManager,
        DomainFactory $domainFactory,
        ProducerInterface $producer,
        Serializer $serializer,
        string $gandiMode,
        string $gandiIdProd,
        string $gandiIdTest
    ) {
        parent::__construct($logger, $entityManager);
        $this->domainFactory = $domainFactory;
        $this->producer = $producer;
        $this->serializer = $serializer;
        /**
         * Are we in test or prod mode ?
         */
        $this->framaGandiId = $gandiMode === 'prod' ? $gandiIdProd : $gandiIdTest;
    }

    /**
     * @param CommandDomain $commandDomain
     */
    public function processCommandDomain(CommandDomain $commandDomain)
    {
        $command = $commandDomain->getCommand();
        $domain = $commandDomain->getDomain();
        $this->entityManager->merge($command);
        $this->entityManager->merge($commandDomain);
        $this->entityManager->merge($domain);
        $this->entityManager->flush();

        $this->logger->debug('Domain contents', [$domain]);

        $user = $this->entityManager->getRepository('AppBundle:User')->find($commandDomain->getDomain()->getUser()->getId());

        /**
         * We create the contacts corresponding to ourself
         */
        $adminContact = new Contact();
        $adminContact->setGandiId($this->framaGandiId);
        $billContact = new Contact();
        $billContact->setGandiId($this->framaGandiId);
        $techContact = new Contact();
        $techContact->setGandiId($this->framaGandiId);

        $adminContact->setContactType(Contact::CONTACT_TYPE_ADMIN);
        $billContact->setContactType(Contact::CONTACT_TYPE_BILL);
        $techContact->setContactType(Contact::CONTACT_TYPE_TECH);

        $this->logger->info('Setting contacts for domain');
        $domain->addContact('admin', $adminContact);
        $domain->addContact('bill', $billContact);
        $domain->addContact('tech', $techContact);

        $this->logger->info('Registering domain');
        $registerData = $this->domainFactory->registerDomain($domain);

        /**
         * Saving zone ID locally
         * TODO: Remove this ?
         */
        /*if (isset($registerData['params']) && isset($registerData['params']['zone_id']) && $registerData['params']['zone_id'] != null) {
            $this->logger->info('Setting DNS Zone');
            $domain->setDnsZone($registerData['params']['zone_id']);
            $this->entityManager->persist($domain);
            $this->entityManager->flush();
        }*/

        $this->logger->info('All actions to register domain done !');

        $this->logger->debug('Setting last domain operation');
        $this->entityManager->merge($domain);
        $this->entityManager->flush();
        $this->logger->debug('Flushing domain');


        /**
         * Command domain is done
         */
        $this->logger->debug("Set command domain with ID " . $commandDomain->getId() . ' to finished');
        $commandDomain->setStatus(CommandDomain::STATUS_PROCESSED);
        $this->entityManager->merge($commandDomain);

        /**
         * Domain is validated
         */
        $this->logger->info('Domain is validated');
        $domain->setStatus(Domain::DOMAIN_VALIDATED);

        /**
         * Setting registered / expiration dates
         */
        $this->logger->info('Setting dates of registration and expiration for domain');
        $domain->setRegisteredAt(new \DateTime());
        $expiringDate = (new \DateTime())->add(new \DateInterval('P1Y'));
        $domain->setExpiresAt($expiringDate);
        // $domain->setUser($user);
        $this->entityManager->merge($domain);
        $this->entityManager->persist($user);

        $this->entityManager->flush();

        $this->producer->publish($this->serializer->serialize($commandDomain, 'json', SerializationContext::create()->setGroups(['domain_register'])));
    }
}
