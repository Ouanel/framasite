<?php

namespace AppBundle\Service;

class StripeKeyService
{

    /**
     * @var string
     */
    private $privateKey;

    /**
     * @var string
     */
    private $publicKey;

    public function __construct(string $stripeApiMode, string $stripeTestApiPrivateKey, string $stripeTestApiPublicKey, string $stripeProdApiPrivateKey = null, string $stripeProdApiPublicKey = null)
    {
        if ($stripeApiMode === 'prod') {
            $this->privateKey = $stripeProdApiPrivateKey;
            $this->publicKey = $stripeProdApiPublicKey;
        } else {
            $this->privateKey = $stripeTestApiPrivateKey;
            $this->publicKey = $stripeTestApiPublicKey;
        }
    }

    /**
     * @return string
     */
    public function getPrivateKey(): string
    {
        return $this->privateKey;
    }

    /**
     * @param string $privateKey
     * @return StripeKeyService
     */
    public function setPrivateKey(string $privateKey): StripeKeyService
    {
        $this->privateKey = $privateKey;
        return $this;
    }

    /**
     * @return string
     */
    public function getPublicKey(): string
    {
        return $this->publicKey;
    }

    /**
     * @param string $publicKey
     * @return StripeKeyService
     */
    public function setPublicKey(string $publicKey): StripeKeyService
    {
        $this->publicKey = $publicKey;
        return $this;
    }
}
