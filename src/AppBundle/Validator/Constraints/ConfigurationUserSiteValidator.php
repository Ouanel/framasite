<?php

namespace AppBundle\Validator\Constraints;

use AppBundle\Async\AsyncAccountDoku;
use AppBundle\Async\AsyncAccountGrav;
use AppBundle\Entity\AbstractSiteUser;
use AppBundle\Entity\Blog\BlogUser;
use AppBundle\Entity\User;
use AppBundle\Entity\Wiki\WikiUser;
use AppBundle\Exception\SiteUserExistsException;
use Psr\Log\LoggerInterface;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ConfigurationUserSiteValidator extends ConstraintValidator
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var AsyncAccountDoku
     */
    private $accountDoku;

    /**
     * @var AsyncAccountGrav
     */
    private $accountGrav;

    /**
     * @var TokenStorageInterface
     */
    private $tokenStorage;

    /**
     * @var User
     */
    private $user;

    public function __construct(LoggerInterface $logger, AsyncAccountGrav $accountGrav, AsyncAccountDoku $accountDoku, TokenStorageInterface $tokenStorage)
    {
        $this->logger = $logger;
        $this->accountDoku = $accountDoku;
        $this->accountGrav = $accountGrav;
        $this->tokenStorage = $tokenStorage;

        $this->user = $this->tokenStorage->getToken()->getUser();
    }

    /**
     * Checks if the passed value is valid.
     *
     * @param AbstractSiteUser $siteUserObject
     * @param Constraint $constraint The constraint for the validation
     */
    public function validate($siteUserObject, Constraint $constraint)
    {
        if ($siteUserObject instanceof BlogUser) {
            $this->accountGrav->setUser($this->user);
            $this->accountGrav->setLogger($this->logger);
            try {
                $this->accountGrav->checkAccount($siteUserObject->getUsername(), $siteUserObject->getSite()->getSubdomain());
            } catch (SiteUserExistsException $e) {
                $this->context->buildViolation($constraint->alreadyExistsUserSiteMessage)
                    ->addViolation();
            }
        } elseif ($siteUserObject instanceof WikiUser) {
            $this->accountDoku->setUser($this->user);
            $this->accountDoku->setLogger($this->logger);
            try {
                $this->accountDoku->checkAccount($siteUserObject->getUsername(), $siteUserObject->getSite()->getSubdomain());
            } catch (SiteUserExistsException $e) {
                $this->context->buildViolation($constraint->alreadyExistsUserSiteMessage)
                    ->addViolation();
            }
        }
    }
}
