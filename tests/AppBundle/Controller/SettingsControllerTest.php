<?php

namespace Tests\AppBundle\Controller;

use Tests\AppBundle\FramasitesTestCase;

class SettingsControllerTest extends FramasitesTestCase {

    public function testLogin()
    {
        $client = $this->getClient();

        $client->request('GET', '/site/new/site');

        $this->assertEquals(302, $client->getResponse()->getStatusCode());
        $this->assertContains('login', $client->getResponse()->headers->get('location'));
    }

    public function testIndex()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/settings');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertCount(1, $crawler->filter('button[id=config_save]'));
        $this->assertCount(1, $crawler->filter('li a[href="/settings/profile"]'));
        $this->assertCount(1, $crawler->filter('li a[href="/settings/password"]'));
    }

    public function testUpdate()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/settings');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->filter('button[id=config_save]')->form();

        $data = [
            'config[locale]' => 'en',
            'config[gandiId]' => 'AB123-GANDI',
        ];

        $client->submit($form, $data);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $crawler = $client->followRedirect();

        $this->assertContains('flashes.settings.config_saved', $crawler->filter('body')->extract(['_text'])[0]);
    }

    public function dataForUpdateFailed()
    {
        return [
            [[
                'config[locale]' => 'en',
                'config[gandiId]' => 'yolo',
            ]],
        ];
    }

    /**
     * @dataProvider dataForUpdateFailed
     * @param array $data
     */
    public function testUpdateFailed(array $data)
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/settings');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->filter('button[id=config_save]')->form();

        $crawler = $client->submit($form, $data);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertGreaterThan(1, $alert = $crawler->filter('body')->extract(['_text']));
        $this->assertContains('gandi.account.incorrect', $alert[0]);
    }

    public function dataForChangePasswordFailed()
    {
        return [
            [
                [
                    'change_passwd[old_password]' => 'nope',
                    'change_passwd[new_password][first]' => '',
                    'change_passwd[new_password][second]' => '',
                ],
                'validator.password_wrong_value',
            ],
            [
                [
                    'change_passwd[old_password]' => 'mypassword',
                    'change_passwd[new_password][first]' => '',
                    'change_passwd[new_password][second]' => '',
                ],
                'This value should not be blank',
            ],
            [
                [
                    'change_passwd[old_password]' => 'mypassword',
                    'change_passwd[new_password][first]' => 'hop',
                    'change_passwd[new_password][second]' => '',
                ],
                'validator.password_must_match',
            ],
            [
                [
                    'change_passwd[old_password]' => 'mypassword',
                    'change_passwd[new_password][first]' => 'hop',
                    'change_passwd[new_password][second]' => 'hop',
                ],
                'settings.password.too_short',
            ],
        ];
    }

    /**
     * @dataProvider dataForChangePasswordFailed
     * @param array $data
     * @param string $expectedMessage
     */
    public function testChangePasswordFailed(array $data, string $expectedMessage)
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/settings/password');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->filter('button[id=change_passwd_save]')->form();

        $crawler = $client->submit($form, $data);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertGreaterThan(1, $alert = $crawler->filter('body')->extract(['_text']));
        $this->assertContains($expectedMessage, $alert[0]);
    }

    public function testChangePassword()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/settings/password');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->filter('button[id=change_passwd_save]')->form();

        $data = [
            'change_passwd[old_password]' => 'mypassword',
            'change_passwd[new_password][first]' => 'mypassword',
            'change_passwd[new_password][second]' => 'mypassword',
        ];

        $client->submit($form, $data);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $crawler = $client->followRedirect();

        $this->assertContains('flashes.settings.password_updated', $crawler->filter('body')->extract(['_text'])[0]);
    }

    public function dataForUserFailed()
    {
        return [
            [
                [
                    'update_user[firstName]' => '',
                    'update_user[lastName]' => '',
                    'update_user[email]' => '',
                ],
                'fos_user.email.blank',
            ],
            [
                [
                    'update_user[firstName]' => '',
                    'update_user[lastName]' => '',
                    'update_user[email]' => 'test',
                ],
                'fos_user.email.invalid',
            ],
        ];
    }

    /**
     * @dataProvider dataForUserFailed
     * @param array $data
     * @param string $expectedMessage
     */
    public function testUserFailed(array $data, string $expectedMessage)
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/settings/profile');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->filter('button[id=update_user_save]')->form();

        $crawler = $client->submit($form, $data);

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $this->assertGreaterThan(1, $alert = $crawler->filter('body')->extract(['_text']));
        $this->assertContains($expectedMessage, $alert[0]);
    }

    public function testUserUpdate()
    {
        $this->logInAs('admin');
        $client = $this->getClient();

        $crawler = $client->request('GET', '/settings/profile');

        $this->assertEquals(200, $client->getResponse()->getStatusCode());

        $form = $crawler->filter('button[id=update_user_save]')->form();

        $data = [
            'update_user[firstName]' => 'Super',
            'update_user[lastName]' => 'Man',
            'update_user[email]' => 'admin@wallabag.io',
        ];

        $client->submit($form, $data);

        $this->assertEquals(302, $client->getResponse()->getStatusCode());

        $crawler = $client->followRedirect();

        $this->assertGreaterThan(1, $alert = $crawler->filter('body')->extract(['_text']));
        $this->assertContains('flashes.settings.profile_updated', $alert[0]);
    }
}